#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Feb 15 17:28:16 2023

@author: alex
"""

import pandas as pd
import matplotlib.pyplot as plt
from pathlib import Path
import os
import numpy as np
import sys
import locale
import platform

try:
    from pimav.pine_analysis import (
        unique_flags,
        pine_export_flagging,
        read_pine_data,
        interactive_plot,
        pine_temperature_binning,
        pine_scatter_plot,
        manual_flagging
    )  # noqa
except ModuleNotFoundError:
    if platform.system() == "Linux":
        # sys.path.append("/home/alex/Dokumente/Python_Modules_GIT")
        sys.path.append("/home/alex/Documents/Python_Modules")
    elif platform.system() == "Windows":
        sys.path.append(r"C:\Users\aboeh\Documents\Python_Custom_Modules")
    from pimav.pine_analysis import (
        unique_flags,
        pine_export_flagging,
        read_pine_data,
        interactive_plot,
        pine_temperature_binning,
        pine_scatter_plot,
        manual_flagging
    )  # noqa

# %%
# set locale for correct spelling in months
locale.setlocale(locale.LC_ALL, 'en_GB')
# set rcParams to default
plt.rcParams.update(plt.rcParamsDefault)
# change font size and activate interactive plots, turn off via plt.ioff()
plt.rcParams.update({'font.size': 22,
                     'interactive': True})
# change current working directory
os.chdir(Path(__file__).parent)

# TODO: change campaign name
campaign = 'ExINP_GVB24'
# TODO: change pine_id name
pine_id = 'PINE-01-A'
# TODO: adjust path to folder structure (where Level 0, Level 1 folders are located)
if platform.system() == 'Linux':
    # path = Path(f'/home/alex/smb/agm-field/Instruments/')  # noqa
    path = Path(f'/media/Data/smb/agm-field/Instruments/')  # noqa
elif platform.system() == 'Windows':
    path = Path('//imkaaf-srv1.imk-aaf.kit.edu/agm-field/Instruments')
# TODO: change path to where calibration files for opc are located
if platform.system() == 'Linux':
    # cal_path = Path('/home/alex/smb/agm-field/Software/Software_Py')
    cal_path = Path('/media/Data/smb/agm-field/Software/Software_Py')
elif platform.system() == 'Windows':
    cal_path = Path('//imkaaf-srv1.imk-aaf.kit.edu/agm-field/Software/Software_Py')

# %%
pine_flags = pine_export_flagging(campaign=campaign,
                                  pine_id=pine_id,
                                  path=path)

pine_data = read_pine_data(campaign=campaign,
                           pine_id=pine_id,
                           path=path,
                           run_flagging=pine_flags)

# %% Custom curation of data
# remove runs that show some issues and are not caught by automatic flagging
run_issues_file_name = Path(os.getcwd()).joinpath(f'{pine_id}_{campaign}_run_issues.txt')
idx_removal = manual_flagging(pine_data, run_issues_file_name)

flag_mask = np.array([pine_data.index.isin(idx_removal)]).any(0)

pine_data['INP_cn_fl_manual'] = np.where(flag_mask,
                                         100, 0)

min_INP, max_INP = pine_data['INP_cn / stdL-1'].min(), pine_data['INP_cn / stdL-1'].max()

# %% Binning data
pine_data_binned_dct, pine_T_binned = pine_temperature_binning(
    pine_data, T_step=2, temporal_resolution=None, min_data_points=20
)
pine_data.loc[:, "temp_bins"] = pine_T_binned
pine_data_binned_dct_1h, _ = pine_temperature_binning(
    pine_data, T_step=2, temporal_resolution="1h", min_data_points=20
)
pine_data_binned_dct_3h, _ = pine_temperature_binning(
    pine_data, T_step=2, temporal_resolution="3h", min_data_points=20
)
pine_data_binned_dct_6h, _ = pine_temperature_binning(
    pine_data, T_step=2, temporal_resolution="6h", min_data_points=20
)
pine_data_binned_all = {
    None: pine_data_binned_dct,
    "60 min": pine_data_binned_dct_1h,
    f"{3 * 60} min": pine_data_binned_dct_3h,
    f"{6 * 60} min": pine_data_binned_dct_6h,
}

# %% Saving data to csv, pickle
Path('./DATA').mkdir(parents=True, exist_ok=True)
pine_data.to_csv(f'./DATA/{campaign}_{pine_id}_Level1.csv')
pine_data.to_pickle(f'./DATA/{campaign}_{pine_id}_Level1.pkl')

# %% Plotting binned data
for temporal_resolution in [None, '60 min', f'{60 * 3} min', f'{60 * 6} min']:
    fig, (ax, ax_res) = pine_scatter_plot(pine_data_binned_all[temporal_resolution],
                                          op_id=None, date=None, start=None, stop=None,
                                          temporal_resolution=temporal_resolution,
                                          title_args={'pine_id': pine_id,
                                                      'campaign': campaign},
                                          title=None)
    Path(f'./PLOTS/ALL/{temporal_resolution}').mkdir(parents=True, exist_ok=True)
    fig.savefig(Path(f'./PLOTS/ALL/{temporal_resolution}').joinpath(
        f'{campaign}_{pine_id}_{temporal_resolution}_Tbinned.png'))
    plt.close('all')

# %% Plotting binned data, weekly
for temporal_resolution in [None, '60 min', f'{60 * 3} min', f'{60 * 6} min']:
    for date, _ in pine_data.groupby(pd.Grouper(freq='W-MON', level=0)):
        if len(_.index) == 0:
            print(f'No data for week of {date} available, generating no plots.')
            continue
        fig, (ax, ax_res) = pine_scatter_plot(pine_data_binned_all[temporal_resolution],
                                              date=None,
                                              start=_.index[0],
                                              stop=_.index[-1],
                                              op_id=None,
                                              temporal_resolution=temporal_resolution,
                                              title=None,
                                              title_args={'pine_id': pine_id,
                                                          'campaign': campaign})
        Path(f'./PLOTS/WEEKLY/{temporal_resolution}').mkdir(parents=True, exist_ok=True)
        fig.savefig(Path(f'./PLOTS/WEEKLY/{temporal_resolution}').joinpath(
            f'{date:%Y%m%d}_{campaign}_{pine_id}_{temporal_resolution}_Tbinned.png'))
        plt.close('all')

# %% Plotting binned data, daily
for temporal_resolution in [None, '60 min', f'{60 * 3} min', f'{60 * 6} min']:
    for date, _ in pine_data.groupby(pine_data.index.date):
        if len(_.index) == 0:
            print(f'No data for {date} available, generating no plots.')
            continue
        fig, (ax, ax_res) = pine_scatter_plot(pine_data_binned_all[temporal_resolution],
                                              date=date,
                                              op_id=None,
                                              temporal_resolution=temporal_resolution,
                                              title=None,
                                              title_args={'pine_id': pine_id,
                                                          'campaign': campaign})
        Path(f'./PLOTS/DAILY/{temporal_resolution}').mkdir(parents=True, exist_ok=True)
        fig.savefig(Path(f'./PLOTS/DAILY/{temporal_resolution}').joinpath(
            f'{date:%Y%m%d}_{campaign}_{pine_id}_{temporal_resolution}_Tbinned.png'))
        plt.close('all')

# %% Interactive plot
flag_lst = unique_flags(pine_flags)

interactive_plot(
    campaign=campaign,
    pine_id=pine_id,
    pine_data=pine_data,
    pine_flags=pine_flags,
    path=path,
    cal_path=cal_path,
    op_id=None,
    show_flag=None,
    remove_error=True,
    remove_manual=True,
    tz="UTC",
)
