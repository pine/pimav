op_id	run_id	comment
8	1	ice visible during flush, T_min above 258 K
8	2	ice visible during flush
47	22	ice visible during flush, pressure increase during flush
47	23	ice visible during flsuh
69	182	ice visible during flush, pressure increase during flush
69	183	ice visible during flush
69	184	ice visible during flush
69	185	ice visible during flush
69	186	ice visible during flush
93	73	ice visible during flush, pressure increase during flush
93	74	ice visible during flush
102	1	T_min above 258 K
102	2	T_min above 258 K
104	4	vertical line through opc
108	225	ice visible during flush, pressure increase during flush
108	226	ice visible during flush
108	227	ice visible during flush
119	-1	no liquid cloud visible
119	198	not sure what is happening here
119	199	not sure what is happening here
119	200	not sure what is happening here
122	1	T_min above 258 K
122	2	T_min above 258 K
122	425	ice visible during flush, pressure increase during flush
122	426	ice visible during flush
124	-318	ice visible during flush, PINE inlet frozen
126	-130	ice visible during flush, PINE inlet frozen
128	-359	ice visible during flush, PINE inlet frozen
131	-17	vertical line through opc, PINE inlet frozen
133	263	ice visible during flush, pressure increase during flush
133	264	ice visible during flush
133	-267	no liquid cloud visible, PINE inlet frozen
135	1	T_min above 258 K
135	2	T_min above 258 K
135	3	T_min above 258 K
135	4	T_min above 258 K
135	6	T_min above 258 K
135	7	ice visible during flush
135	8	ice visible during flush
135	10	vertical line through opc
135	-11	ice visible during flush, PINE inlet frozen
162	129	ice visible during flush, pressure increase during flush
162	130	ice visible during flush
162	131	ice visible during flush
162	132	ice visible during flush
162	133	ice visible during flush
178	43	ice visible during flush
178	370	ice visible during flush, pressure decrease during flush
178	371	ice visible during flush
188	182	ice visible during flush, pressure increase during flush
188	183	ice visible during flush
203	24	ice visible during flush, pressure increase during flush
203	25	ice visible during flush
210	28	ice visible during flush, pressure increase during flush
210	29	ice visible during flush
228	541	possible hom. freezing
228	542	possible hom. freezing
228	543	possible hom. freezing
228	544	possible hom. freezing
228	545	possible hom. freezing
228	546	possible hom. freezing
228	547	possible hom. freezing
228	548	possible hom. freezing
228	549	possible hom. freezing
