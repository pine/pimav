Functions
=========

Creating data objects from Level 1 data
---------------------------------------

The Level 1 data of the PIA project is split into several files, one for each operation. The data can be collected and merged with the following function.

.. autofunction:: pimav.pine_analysis.read_pine_data

Flag removal
------------

Data can be put in context by using the flag data generated by PIA.

.. autofunction:: pimav.pine_analysis.pine_quality_removal

Binning of PINE data
--------------------

.. autofunction:: pimav.pine_analysis.pine_temperature_binning

Plotting of PINE data
---------------------

.. autofunction:: pimav.pine_analysis.pine_scatter_plot

Interactive plotting
--------------------

.. autofunction:: pimav.pine_analysis.interactive_plot

Additional low level help functions
-----------------------------------

.. autofunction:: pimav.pine_analysis.read_welas_spd
.. autofunction:: pimav.pine_analysis.extending_lists
.. autofunction:: pimav.pine_analysis.unique_flags
