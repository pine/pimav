PIMAV
=====

PIMAV is a Python library for timeseries analysis and visualisation of PINE data. It also features implementation of quality control to use together with the [PIA](https://codebase.helmholtz.cloud/pine/pia_software) software.

PIMAV works with the same environment as specified in PIA, please refer to the PIA documentation on how to create a new virtual environment.

Refer to the documentation located under docs/build/html (WIP!). Examples are found inside the Examples folder.
