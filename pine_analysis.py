#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 12 17:32:25 2023

@author: alex
"""

import typing
from datetime import datetime
import pandas as pd
import platform


def pine_export_flagging(campaign: str,
                         pine_id: str,
                         path: typing.TextIO,
                         short_name: bool = False) -> dict:
    """
    Function to export flag data of PINE.

    Parameters
    ----------
    campaign : str
        Name of campaign.
    pine_id : str
        ID of PINE, i.e. PINE-04-01.
        this value should be set to True. The default is False.
    path : TextIO
        Path is the location of the instrument folder.
        It can either be specified as a str:
            '/data/of/pine'
        or as a Path object with the pathlib library:
            Path('/data/of/pine)
    short_name : bool, optional
        If the filename contains the short_name instead of the full pine_id,

    Returns
    -------
    flag_dct : dict
        Dictionary of flags.

    """
    from pathlib import Path, PurePath
    import pandas as pd
    import re

    if isinstance(path, PurePath):
        pine_path = path
    elif isinstance(path, str):
        pine_path = Path(path)
    pine_quality_path = pine_path.joinpath(f'{pine_id}/{campaign}/Quality_Control')
    pine_quality_files = [_ for _ in pine_quality_path.glob('*flags.log')]

    flag_dct = {}
    for i, file in enumerate(pine_quality_files):
        if file.stat().st_size == 0:
            continue
        op_id_raw = re.search(r'op_id_\d+_', file.stem).group(0)
        op_id = int(re.search(r'\d+', op_id_raw).group(0))
        flag_df = pd.read_table(file, header=None)
        try:
            flag_df = flag_df.iloc[:, 0].str.split(',', expand=True)
            flag_df_0 = flag_df.iloc[:, 0].str.split(':', expand=True)
            flag_df_1 = flag_df.iloc[:, 1].str.split(':', expand=True)
        except IndexError:
            flag_df = flag_df.iloc[:, 0].str.split(': ', expand=True)
            flag_df_0 = flag_df.iloc[:, 0].str.split(':', expand=True)
            flag_df_1 = flag_df.iloc[:, 1].str.split(':', expand=True)
        flag_df = pd.concat([flag_df_0, flag_df_1], axis=1, ignore_index=True)
        flag_df.rename({0: 'type',
                        1: 'run_id',
                        2: 'phase',
                        3: 'description'}, axis=1, inplace=True)
        flag_df['run_id'] = flag_df['run_id'].map(lambda x: int(re.search(r'\d+', x).group(0)))
        flag_dct[op_id] = flag_df

    return flag_dct


def unique_flags(pine_flags: dict) -> list:
    """
    Helper function that provides a list of unique pine flags for a given
    dictionary of a campaign or similar.

    Parameters
    ----------
    pine_flags : dict
        Dictionary of pine_flags. Keys represent the operation id,
        whereas the elements are pandas.DataFrames containing runwise
        information on the flags.

    Returns
    -------
    list
        List of all unique flags.

    """
    unique_flag_lst = []
    for op_id_, flag in pine_flags.items():
        unique_flag_lst.append(flag['description'].unique())
    unique_flag_lst = sorted(list(set([uni_flag for sublist in unique_flag_lst
                                       for uni_flag in sublist])))

    return unique_flag_lst


def extending_lists(faulty_runs: list,
                    max_run_number: int) -> list:
    """
    Helper function for splitting and expanding list elements.
    If you have a list a:
    a = [1, 2, 3, 7, 8, 20, 26]
    this function will split the list into sublists
    b = [[1, 2, 3], [7, 8], [20], [26]]
    and extend each sublist to the left and to the right
    c = [[1, 2, 3, 4], [6, 7, 8, 9], [19, 20, 21], [25, 26, 27]]
    unless the number is 1 or the number is larger than the max_run_number.
    Finally the list is combined into one with a list comprehension, taking
    the set to remove duplicates and finally transformed into a list again
    and returned.
    This function is used with runs, where particles are visible during
    flush above the ice threshold. By extending the number of runs to
    the left and right by one, one will also catch the start and end of
    this occurence.

    Parameters
    ----------
    faulty_runs : list
        List of runs, where a certain concentration of particles is visible
        above the ice threshold during flush.
    max_run_number : int
        The total number of runs for a given operation.

    Returns
    -------
    list
        Extends list of runs that should be flagged and checked.

    """
    import numpy as np
    lst = faulty_runs
    if len(lst) == 0:
        return []

    idx_to_split_at = [idx+1 for idx in range(len(lst)-1) if np.diff(lst)[idx] > 1]
    idx_to_split_at.insert(0, 0)
    idx_to_split_at.append(len(lst))

    split_lst = [lst[idx_to_split_at[i]:idx_to_split_at[i+1]]
                 for i in range(len(idx_to_split_at)-1)]

    for lst_in_split_lst in split_lst:
        if lst_in_split_lst[0] == 1:
            pass
        if lst_in_split_lst[0] > 1:
            lst_in_split_lst.insert(0, lst_in_split_lst[0] - 1)
        if lst_in_split_lst[-1] < max_run_number:
            lst_in_split_lst.append(lst_in_split_lst[-1] + 1)

    return list(set([element for sublist in split_lst for element in sublist]))


def pine_temperature_binning(pine_data: pd.DataFrame,
                             T_step: int | float = 2,
                             temporal_resolution: str = '1h',
                             T_offset: float = 0,
                             min_data_points: int = 20) -> dict:
    """
    Takes PINE data as returned by the function read_pine_data()
    and performs binning into temperature bins and then into
    temporal bins.

    Parameters
    ----------
    pine_data : pd.DataFrame
        PINE data as returned by the function read_pine_data().
    T_step : int | float, optional
        Size of temperature bins in K.
        The default 2.
    temporal_resolution : str, optional
        Temporal resolution of temporal bins. Can be given like
        '30min', '5h' or '1d', etc.
        The default is '1h'.
    min_data_points : int, optional
        Minimum amount of data points to create a separate
        temperature bin. If there are less than min_data_points
        in a temperature bin, this data is not added to the
        final dictionary.
        The default is 20.

    Returns
    -------
    dict
        Dictionary of PINE data with keys being the temperature
        bins and values the corresponding data.

    """
    import math
    import numpy as np
    T_min, T_max = pine_data['T_min / K'].min(), pine_data['T_min / K'].max()
    T_list = np.arange(math.floor(T_min), math.ceil(T_max)+1, step=T_step) + T_offset

    pine_data_copy = pine_data.copy(deep=True)

    pine_T_binned = pd.cut(pine_data['T_min / K'], bins=T_list)
    pine_data_copy.loc[:, 'temp_bins'] = pine_T_binned
    pine_data_binned_dct = {}
    for bins, p_data in pine_data_copy.groupby(pine_data_copy['temp_bins'], observed=False):
        if len(p_data) > min_data_points:
            if isinstance(temporal_resolution, type(None)):
                pine_data_binned_dct[bins] = p_data.drop('temp_bins', axis=1)
            elif isinstance(temporal_resolution, str):
                pine_data_binned_dct[bins] = (p_data.drop('temp_bins', axis=1)
                                              .resample(temporal_resolution).mean())
    return pine_data_binned_dct, pine_T_binned


def read_pine_data(campaign: str,
                   pine_id: str,
                   path: typing.TextIO,
                   short_name: bool = False,
                   logbook_name: str = None,
                   logbook_header: bool = True,
                   run_flagging: dict = False,
                   op_type: str | int = False,
                   utc: bool = True) -> pd.DataFrame:
    """
    Read pine data from server.
    Adding of flags to runs with flag dictionary.
    An op_type can be specified as well.

    Parameters
    ----------
    campaign : str
        Name of campaign.
    pine_id : str
        ID of PINE, i.e. PINE-04-01.
    path : TextIO
        Path is the location of the instrument folder.
        It can either be specified as a str:
            '/data/of/pine'
        or as a Path object with the pathlib library:
            Path('/data/of/pine)
    short_name : bool, optional
        If the filename contains the short_name instead of the full pine_id,
        this value should be set to True. The default is False.
    logbook_name : str, optional
        Name of the logbook. The default is None.
    logbook_header : bool, optional
        Does the logbook have a header? The default is True.
    run_flagging : dict, optional
        Flag some runs after quality control. The default is None.
    op_type : str | int, optional
        Do you want to get other modes, i.e. cirrus = 4 or test = 5?
        op_type=4, op_type='cirrus'
        returns only cirrus experiments.
    utc : bool, optional. The defauls is True.
        Whether the time is given in UTC or not. If False timezone-naive DatetimeIndex is
        returned.

    Returns
    -------
    pine_data : pd.DataFrame
        PINE data for a specific campaign.

    """
    import pandas as pd
    import re
    import numpy as np
    from pathlib import Path, PurePath

    pine_id_dict = {'PINE-04-01': 'PINE-401',
                    'PINE-04-02': 'PINE-402',
                    'PINE-04-03': 'PINE-403'}
    if short_name:
        pine_id_short = pine_id_dict[pine_id]
    else:
        pine_id_short = pine_id
    if isinstance(path, PurePath):
        pine_path = path.joinpath(f'{pine_id}/{campaign}')
    elif isinstance(path, str):
        pine_path = Path(path).joinpath(f'{pine_id}/{campaign}')

    path_operation = pine_path.joinpath(f"pfo_{pine_id}_{campaign}.txt")

    operation = pd.read_csv(path_operation, sep=r'\t', engine='python',
                            parse_dates=['dto_start', 'dto_stop'])

    if not logbook_name:
        logbook = pd.read_excel(pine_path.joinpath(f'Logbook_{campaign}.xlsx'),
                                engine='openpyxl')
    else:
        logbook = pd.read_excel(pine_path.joinpath(logbook_name),
                                engine='openpyxl')
    if logbook_header:
        pass
    else:
        logbook.columns = logbook.iloc[0, :]
    logbook.drop(0, inplace=True)
    if not op_type:
        try:
            logbook = logbook[logbook['opreation type (#)'].notna()]
            op_ids = logbook['# operation'][logbook['opreation type (#)'].str.endswith((
                '(2)', '(3)'))].to_list()
        except KeyError:
            logbook = logbook[logbook['operation type (#)'].notna()]
            op_ids = logbook['# operation'][logbook['operation type (#)'].str.endswith((
                '(2)', '(3)'))].to_list()
    else:
        if op_type == 'background':
            op_type = 1
        elif op_type == 'cirrus':
            op_type = 4
        elif op_type == 'test':
            op_type = 5
        try:
            logbook = logbook[logbook['opreation type (#)'].notna()]
            op_ids = logbook['# operation'][logbook['opreation type (#)'].str.endswith((
                f'({op_type})'))].to_list()
        except KeyError:
            logbook = logbook[logbook['operation type (#)'].notna()]
            op_ids = logbook['# operation'][logbook['operation type (#)'].str.endswith((
                f'({op_type})'))].to_list()

    operation = operation.loc[operation['op_id'].isin(op_ids), :]

    pine_data_list = []
    for index in operation['op_id']:
        path_pfr = pine_path.joinpath("raw_Data")
        if type(operation.loc[operation['op_id'] == index]['df_pfr'].to_numpy()[0]) != str:
            continue
        try:
            data_pfr = pd.read_csv(path_pfr.joinpath(
                operation.loc[operation['op_id'] == index, 'df_pfr'].to_numpy()[0]),
                sep=r'\t', engine='python',
                parse_dates=['time start', 'time expansion',
                             'time refill', 'time end'])
        except FileNotFoundError:
            _path = path_pfr.joinpath(
                operation.loc[operation['op_id'] == index]['df_pfr'].to_numpy()[0])
            print(f'File: {str(_path)} not found.')
            continue
        path_ice = pine_path.joinpath("L1_Data/exportdata/exportdata_ice")

        try:
            with open(
                path_ice.joinpath(
                    f"{pine_id_short}_{campaign}_op_id"
                    f"_{operation.loc[operation['op_id'] == index]['op_id'].to_numpy()[0]}"
                    "_ice.txt")) as f:
                firstline = f.readline().rstrip()
                version_number = re.search(r'\d.\d.\d', firstline).group()
                major_version = int(version_number.split('.')[0])
                minor_version = int(version_number.split('.')[1])
                patch_version = int(version_number.split('.')[2])
                if major_version >= 2:
                    if minor_version == 1:
                        header = 18
                    elif minor_version == 2:
                        header = 18
                    elif patch_version == 2:
                        header = 18
                    elif patch_version <= 1:
                        header = 20
                else:
                    header = 9
            data_ice = pd.read_csv(
                path_ice.joinpath(
                    f"{pine_id_short}_{campaign}_op_id"
                    f"_{operation.loc[operation['op_id'] == index]['op_id'].to_numpy()[0]}"
                    "_ice.txt"),
                header=header, sep='\t', engine='python')
        except FileNotFoundError:
            _ice_path = path_ice.joinpath(
                f"{pine_id_short}_{campaign}_op_id"
                f"_{operation.loc[operation['op_id'] == index]['op_id'].to_numpy()[0]}"
                "_ice.txt")
            print(f'File: {str(_ice_path)} not found.')
            continue

        data_ice.set_index('run_id', inplace=True)

        if isinstance(run_flagging, dict):
            try:  # flag ERROR and WARNING flags
                run_rem = run_flagging[index]
                run_lst_error = run_rem.loc[run_rem['type']
                                            .str.contains('ERROR'), 'run_id'].unique()
                run_lst_warning = run_rem.loc[run_rem['type']
                                              .str.contains('WARNING'), 'run_id'].unique()
                data_ice['INP_cn_0_fl_warning'] = np.where(data_ice.index.isin(run_lst_warning),
                                                           1, 0)
                data_ice['INP_cn_0_fl_error'] = np.where(data_ice.index.isin(run_lst_error),
                                                         10, 0)
            except KeyError:
                data_ice['INP_cn_0_fl_warning'] = 0
                data_ice['INP_cn_0_fl_error'] = 0
        else:
            # fill with nan if no dict is provided for the flags
            data_ice['INP_cn_0_fl_warning'] = np.nan
            data_ice['INP_cn_0_fl_error'] = np.nan

        pine_INP = pd.DataFrame({})
        pine_INP['op_id'] = [index
                             for run in data_pfr['RUN'].to_numpy()
                             if run in data_ice.index.to_numpy()]
        pine_INP['run_id'] = [int(run)
                              for run in data_pfr['RUN'].to_numpy()
                              if run in data_ice.index.to_numpy()]
        pine_INP['T_min / K'] = [data_ice.loc[int(run), 'T_min']
                                 for run in data_pfr['RUN'].to_numpy()
                                 if run in data_ice.index.to_numpy()]
        pine_INP['INP_cn / stdL-1'] = [data_ice.loc[int(run), 'INP_cn_0']
                                       for run in data_pfr['RUN'].to_numpy()
                                       if run in data_ice.index.to_numpy()]
        pine_INP['INP_cn_fl_warning'] = [data_ice.loc[int(run), 'INP_cn_0_fl_warning']
                                         for run in data_pfr['RUN'].to_numpy()
                                         if run in data_ice.index.to_numpy()]
        pine_INP['INP_cn_fl_error'] = [data_ice.loc[int(run), 'INP_cn_0_fl_error']
                                       for run in data_pfr['RUN'].to_numpy()
                                       if run in data_ice.index.to_numpy()]
        pine_INP['INP_cn_flush / stdL-1'] = [data_ice.loc[int(run), 'INP_cn_flush']
                                             for run in data_pfr['RUN'].to_numpy()
                                             if run in data_ice.index.to_numpy()]
        pine_INP.index = [data_pfr.loc[data_pfr['RUN'] == int(run), 'time refill'].to_numpy()[0]
                          for run in data_pfr['RUN'].to_numpy()
                          if run in data_ice.index.to_numpy()]
        pine_data_list.append(pine_INP)

    # remove empty list elements
    pine_data_list = [_ for _ in pine_data_list if not _.empty]
    pine_data = pd.concat(pine_data_list)
    pine_data[["op_id", "run_id"]] = pine_data[["op_id", "run_id"]].apply(
        lambda _: pd.to_numeric(_, downcast='integer'))
    pine_data.sort_index(inplace=True)
    if utc:
        pine_data.index = pine_data.index.tz_localize('UTC')
        pine_data.index.name = 'UTC'
    else:
        pass

    return pine_data


def read_welas_spd(file: typing.TextIO,
                   calibration_file: typing.TextIO,
                   tz: str = 'UTC') -> pd.DataFrame:
    """
    Read welas single particle file and map bin number to particle diameter.

    Parameters
    ----------
    file : TextIO
        File containing single particle data of welas.
    calibration_file : TextIO
        File containing calibration data of welas.
    tz : str
        Timezone of welas single particle data.

    Returns
    -------
    data : pd.DataFrame
        Single particle data mapped to particle diameter with a given calibration file.

    """
    import pandas as pd

    calibration_table = pd.read_csv(calibration_file, delimiter='\t',
                                    names=['Bin number', 'Size / um'])

    data = pd.read_csv(file, sep='\t', names=['Datetime', 'dt / s',
                                              'bin / -', 'Ti / us'],
                       encoding='iso8859-15')
    data.index = (pd.to_datetime(data['Datetime'])
                  + pd.to_timedelta(data['dt / s'], 's'))
    if tz == 'UTC':
        data.index = data.index.tz_localize(tz)
    else:
        data.index = data.index.tz_localize(tz)
        data.index = data.index.tz_convert('UTC')
    data.drop([data.columns[0]], inplace=True, axis=1)

    data['dp / um'] = data['bin / -'].map(calibration_table.set_index('Bin number')['Size / um'])
    return data


def pine_scatter_plot(pine_data_binned: dict,
                      op_id: int = None,
                      date: datetime = None,
                      start: datetime = None,
                      stop: datetime = None,
                      temporal_resolution: str = '1h',
                      title: str = None,
                      title_args: dict = {}) -> tuple:
    """
    Function that creates a typical scatter plot of PINE data.

    Parameters
    ----------
    pine_data_binned : dict
        Temperature binned pine data.
    op_id : int, optional
        Operation ID that should be plotted.
        The default is None.
    date : datetime, optional
        Date of plot, useful if you want to plot daily data.
        The default is None.
    start : datetime, optional
        Start datetime of plot. The default is None.
    stop : datetime, optional
        Stop datetime of plot. The default is None.
    temporal_resolution : str, optional
        Temporal resolution for the data. The default is '1h'.
    title : str, optional
        Title can be chosen freely. If None a title is automatically
        created using information on PINE id and campaign.
        The default is None.
    title_args: dict, optional
        Title arguments dictionary containing pine_id as well as
        campaign name:
        title_args = {"pine_id": "PINE-04-02", "campaign": "ExINP_ZEP"}
        The default is None.

    Returns
    -------
    tuple
        Tuple containing the created figure as well as both axis.
        Figure object can be used to save the figure, while the
        axis objects can be used to manipulate the plots.

    """
    import matplotlib.pyplot as plt
    import numpy as np
    import matplotlib as mpl
    import matplotlib.colors as mcolors
    import matplotlib.dates as mdates
    from matplotlib import ticker

    cmap = mpl.colormaps["viridis"]
    colors = cmap(np.linspace(0, 1., num=len(np.unique(pine_data_binned.keys())[0])))
    bounds = np.arange(list(pine_data_binned.keys())[0].left,
                       list(pine_data_binned.keys())[-1].right+1,
                       next(iter(pine_data_binned)).length)
    norm = mcolors.BoundaryNorm(bounds, cmap.N)

    min_inp, max_inp = [], []

    fig, (ax, ax_res) = plt.subplots(2, 1, sharex=True,
                                     height_ratios=[5, 1],
                                     figsize=(16, 9),
                                     constrained_layout=True)
    for i, (key, items) in enumerate(pine_data_binned.items()):
        if isinstance(op_id, type(None)):
            pass
        elif isinstance(op_id, (int, float)):
            items = items.loc[items['op_id'] == float(op_id), :]
        if isinstance(date, type(None)):
            pass
        else:
            items = items.loc[items.index.date == date, :]
        if isinstance(start, type(None)):
            pass
        else:
            items = items.loc[start:stop, :]
        ax.scatter(items.index, items['INP_cn / stdL-1'],
                   color=colors[i])
        min_inp.append(items.loc[items['INP_cn / stdL-1'] > 0, 'INP_cn / stdL-1'].min())
        max_inp.append(items.loc[items['INP_cn / stdL-1'] > 0, 'INP_cn / stdL-1'].max())
        items = items.loc[items['INP_cn / stdL-1'] == 0, :]
        ax_res.scatter(items.index, items['INP_cn / stdL-1'],
                       color=colors[i])
    sm = plt.cm.ScalarMappable(cmap=cmap, norm=norm)
    sm.set_array([])
    plt.colorbar(sm, ax=(ax, ax_res), ticks=bounds, label='$T$ / K', pad=0.005)
    locator = mdates.AutoDateLocator()
    formatter = mdates.ConciseDateFormatter(locator)
    ax_res.xaxis.set_major_locator(locator)
    ax_res.xaxis.set_major_formatter(formatter)
    ax.set_yscale('log')

    try:
        ax.set_ylim(0.8 * np.nanmin(min_inp), 1.2 * np.nanmax(max_inp))
    except ValueError as e:
        print(f'{e}. Using automatic axis limits.')
    for axis in [ax, ax_res]:
        axis.set_ylabel(r'$c_\mathrm{INP}$ / l$_\mathrm{std}^{-1}$')
        axis.grid()
    if isinstance(title, type(None)):
        if all(k in title_args for k in ('pine_id', 'campaign')):
            if isinstance(temporal_resolution, type(None)):
                title = 'raw data'
            elif isinstance(temporal_resolution, str):
                title = f'{temporal_resolution} mov. avg.'
            ax.set_title(f'{title_args["pine_id"]} ({title_args["campaign"]}): {title} (T_binned)',
                         pad=10)
    elif isinstance(title, str):
        ax.set_title(title, pad=10)
    ax_res.set_xlabel('UTC')
    ax_res.yaxis.set_major_locator(ticker.FixedLocator([0]))
    ticks = ax_res.get_yticks()
    dic = {0.0: f'< {min(min_inp):.2f}'}
    labels = [ticks[i] if t not in dic.keys() else dic[t] for i, t in enumerate(ticks)]
    ax_res.set_yticklabels(labels)
    return fig, (ax, ax_res)


def interactive_plot(campaign: str,
                     pine_id: str,
                     pine_data: pd.DataFrame,
                     pine_flags: dict,
                     path: typing.TextIO,
                     cal_path: typing.TextIO,
                     op_id: int = None,
                     show_flag: str = ' Tw3 out of range',
                     remove_error: bool = False,
                     remove_manual: bool = False,
                     tz: str = 'UTC') -> list:
    """
    Function that creates an interactive plot of PINE data.
    Each scatter point can be clicked, which will produce
    a separate plot of the given expansion.
    Annotations are added and can be made invisible with
    delete and visible with insert.

    Parameters
    ----------
    campaign : str
        Name of the campaign.
    pine_id : str
        PINE ID as in PINE-04-01 or similar.
    pine_data : pd.DataFrame
        PINE data as returned by the function read_pine_data().
    pine_flags : dict
        Flag dictionary as returned by the function pine_quality_removal().
    path : TextIO
        Path is the location of the instrument folder.
        It can either be specified as a str:
            '/data/of/pine'
        or as a Path object with the pathlib library:
            Path('/data/of/pine)
    cal_path : typing.TextIO
        Calibration path for calibration file.
    op_id : int, optional
        Operation ID to plot. This way one can take a more detailed
        look at just a single operation. The default is None.
    show_flag : str, optional
        Specific flags can be made visible in the plot.
        Note that there is usually a space in front of the flag name.
        The default is ' Tw3 out of range'.
    error : bool, optional
        If True, pine data will only contain data where no error occured.
        The default is False.
    manual : bool, optional
        If True, pine data will only contain data where no manual flag occured.
        The default is False.

    Returns
    -------
    unique_flag_lst : list
        If the flag specified is not one of the available ones, you get
        a list of available flags to chose from. A unique flag list can
        be generated prior with the function unique_flags().

    """
    import matplotlib.pyplot as plt
    import matplotlib.dates as mdates
    from pathlib import Path, PurePath

    if isinstance(path, PurePath):
        pine_path = path.joinpath(f'{pine_id}/{campaign}')
    elif isinstance(path, str):
        pine_path = Path(path).joinpath(f'{pine_id}/{campaign}')

    pine_data_raw = pine_data.copy(deep=True)
    pine_data = pine_data.copy(deep=True)
    if remove_error:
        pine_data = pine_data.loc[pine_data['INP_cn_fl_error'] == 0, :]
    if remove_manual:
        pine_data = pine_data.loc[pine_data['INP_cn_fl_manual'] == 0, :]

    if op_id:
        start = pine_data_raw.loc[pine_data_raw['op_id'] == op_id, :].index[0]
        stop = pine_data_raw.loc[pine_data_raw['op_id'] == op_id, :].index[-1]
        start = start - pd.Timedelta('5min')
        stop = stop + pd.Timedelta('5min')

    if show_flag:
        flag_selected = show_flag

        unique_flag_lst = unique_flags(pine_flags)
        if flag_selected not in unique_flag_lst:
            print('Flag is not defined. Choose one from the following list: \n')
            return unique_flag_lst

        flags = [(key, flag.loc[flag['description'] == flag_selected, 'run_id'].to_list())
                 for key, flag in pine_flags.items()]

        flag_idx = []
        for op_id_, run_lst in flags:
            mask = pine_data_raw['op_id'] == op_id_
            mask2 = pine_data_raw['run_id'].isin(run_lst)
            flag_idx.append(pine_data_raw.loc[(mask & mask2), :].index)

        flag_idx = [f for sublist in flag_idx for f in sublist]

    fig, ax = plt.subplots(figsize=(16, 9), constrained_layout=True)
    c = ax.scatter(x=pine_data.index,
                   y=pine_data['INP_cn / stdL-1'],
                   c=pine_data['T_min / K'])
    ax.scatter(x=pine_data_raw.index,
               y=pine_data_raw['INP_cn / stdL-1'],
               c='grey', alpha=0.3, zorder=0,
               picker=True)

    if show_flag:
        flag_marker = dict(marker='o',
                           linestyle='',
                           markersize=c.get_sizes()[0]**(0.5),
                           color='None',
                           markerfacecolor='None',
                           markeredgecolor='red')
        ax.plot(pine_data_raw.loc[pine_data_raw.index.isin(flag_idx), :].index,
                pine_data_raw.loc[pine_data_raw.index.isin(flag_idx), 'INP_cn / stdL-1'],
                **flag_marker)
        ax.set_title(f'Flag "{flag_selected.strip()}" marked with red edges.')
    fig.colorbar(c, ax=ax, label='$T$ / K')
    locator = mdates.AutoDateLocator()
    formatter = mdates.ConciseDateFormatter(locator)
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)
    ax.set_yscale('symlog', linthresh=1)
    if op_id:
        ax.set_xlim(start, stop)
    ax.grid()
    ax.set_xlabel('Date UTC')
    ax.set_ylabel(r'$c_\mathrm{INP}$ / l$_\mathrm{std}^{-1}$')

    def on_key(event):
        """
        Function to be bound to the key press event
        If the key pressed is delete and there is an annotation,
        all annotations have their visibility set to False.
        If the key pressed is insert, all annotations are set
        to visible again.
        """
        import matplotlib as mpl
        if event.key == u'delete':
            ax = plt.gca()
            for child in ax.get_children():
                if isinstance(child, mpl.text.Annotation):
                    child.set(visible=False)
                ax.figure.canvas.draw()
        if event.key == u'insert':
            ax = plt.gca()
            for child in ax.get_children():
                if isinstance(child, mpl.text.Annotation):
                    child.set(visible=True)
                ax.figure.canvas.draw()

    def onpick(event):
        import configparser

        ind = event.ind[0]
        idx = pine_data_raw.index[ind]

        start_m2 = pine_data_raw.iloc[pine_data_raw.index.get_loc(idx)-2, :].name
        start_m1 = pine_data_raw.iloc[pine_data_raw.index.get_loc(idx)-1, :].name
        start_0 = pine_data_raw.iloc[pine_data_raw.index.get_loc(idx), :].name  # same as idx
        stop_p1 = pine_data_raw.iloc[pine_data_raw.index.get_loc(idx)+1, :].name

        inp = pine_data_raw.loc[idx, 'INP_cn / stdL-1']
        Tmin = pine_data_raw.loc[idx, 'T_min / K']
        op_id = int(pine_data_raw.loc[idx, 'op_id'])
        run_id = int(pine_data_raw.loc[idx, 'run_id'])
        try:
            flag = pine_flags[op_id].loc[pine_flags[op_id]['run_id'] == run_id,
                                         :]
        except KeyError:
            # i.e. no op_id in pine_flags dictionary
            flag = pd.DataFrame(data=None,
                                columns=['type', 'run_id', 'phase', 'description'])
        phase = flag['phase'].to_list()
        descr = flag['description'].to_list()
        fl_type = flag['type'].to_list()
        try:
            flag_info = [f'{ph.strip()} ({ty.strip()}): {dc.strip()}'
                         for ph, dc, ty in zip(phase, descr, fl_type)]
        except AttributeError:
            flag_info = []
        if len(flag_info) > 1:
            flag_info = '\n'.join(flag_info)
            info_string = (f'Time: {idx:%Y-%m-%d %H:%M:%S}'
                           '\n'
                           fr"$c_\mathrm{{INP}}$ = {inp:.2f} l$_\mathrm{{std}}^{{-1}}$"
                           '\n'
                           fr"$T_\mathrm{{min}}$ = {Tmin:.2f} K"
                           '\n'
                           + flag_info)
        elif len(flag_info) == 1:
            flag_info = flag_info[0]
            info_string = (f'Time: {idx:%Y-%m-%d %H:%M:%S}'
                           '\n'
                           fr"$c_\mathrm{{INP}}$ = {inp:.2f} l$_\mathrm{{std}}^{{-1}}$"
                           '\n'
                           fr"$T_\mathrm{{min}}$ = {Tmin:.2f} K"
                           '\n'
                           + flag_info)
        else:
            flag_info = ''
            info_string = (f'Time: {idx:%Y-%m-%d %H:%M:%S}'
                           '\n'
                           fr"$c_\mathrm{{INP}}$ = {inp:.2f} l$_\mathrm{{std}}^{{-1}}$"
                           '\n'
                           fr"$T_\mathrm{{min}}$ = {Tmin:.2f} K")
        ax = plt.gca()
        ax.annotate(info_string, xy=(idx, inp), xytext=(-30, 40), xycoords='data',
                    textcoords='offset points', ha='center',
                    bbox=dict(boxstyle="round", fc="gray", ec="gray",
                              alpha=0.7), arrowprops=dict(arrowstyle="->"))
        print('Make annotations invisible by pressing delete, ',
              'make them visible again by pressing insert.')
        plt.gcf().canvas.draw_idle()

        def draw_diagnostic(idx):
            hk_path = pine_path.joinpath('raw_data/housekeeping'
                                         f'/df_{pine_id}_{campaign}_opid-{op_id}_instrument.txt')
            df_hk_instrument = pd.read_csv(hk_path, header=0,
                                           parse_dates=['date and time'], sep='\t')
            df_hk_instrument.set_index('date and time', inplace=True)
            if tz == 'UTC':
                df_hk_instrument.index = df_hk_instrument.index.tz_localize('UTC')
            else:
                df_hk_instrument.index = df_hk_instrument.index.tz_localize(tz)
                df_hk_instrument.index = df_hk_instrument.index.tz_convert('UTC')

            if pine_path.joinpath(
                    'L1_Data/exportdata/ice_threshold'
                    rf'/{pine_id}_{campaign}_op_id_{op_id}_ice_threshold_manually.txt').exists():
                ice_threshold = pd.read_csv(pine_path.joinpath(
                    'L1_Data/exportdata/ice_threshold'
                    rf'/{pine_id}_{campaign}_op_id_{op_id}_ice_threshold_manually.txt'),
                    sep='\t', header=0)
            else:
                ice_threshold = pd.read_csv(pine_path.joinpath(
                    'L1_Data/exportdata/ice_threshold'
                    rf'/{pine_id}_{campaign}_op_id_{op_id}_ice_threshold.txt'),
                    sep='\t', header=1)

            cf_file = pine_path.joinpath(
                f'operation setup/cf_{pine_id}_{campaign}_opid-{op_id}.txt')
            with open(cf_file, encoding="iso-8859-15") as f:
                _lines = ''.join(f.readlines()[2:])
                configParser = configparser.RawConfigParser()
                configParser.read_string(_lines)
                try:
                    calibration_file = cal_path.joinpath(
                        configParser['OPC 1']['calib-file'])
                    opc_id = configParser['OPC 1']['opc-id']
                except KeyError:  # new LV software used OPC instead of OPC 1 as key and size file instead of calib file.
                    calibration_file = cal_path.joinpath(
                        configParser['OPC']['size-file'])
                    opc_id = 'OPC'

            if run_id >= 2:
                spd_path = [pine_path.joinpath(f'raw_data/{opc_id}'
                                               f'/df_{pine_id}_{campaign}'
                                               f'_opid-{op_id}_runid-{run_id}_opc.txt')
                            for run_id in [run_id-1, run_id, run_id+1]]
            elif run_id == 1:
                spd_path = [pine_path.joinpath(f'raw_data/{opc_id}'
                                               f'/df_{pine_id}_{campaign}'
                                               f'_opid-{op_id}_runid-{run_id}_opc.txt')
                            for run_id in [run_id, run_id+1]]
            spd_df = []
            for f in spd_path:
                try:
                    df = read_welas_spd(f, calibration_file, tz)
                except FileNotFoundError:
                    if platform.system() == 'Linux':
                        f = str(f).replace(f'raw_data/{opc_id}',
                                           f'raw_data/{opc_id}/OP {op_id}')
                    elif platform.system() == 'Windows':
                        f = str(f).replace(f'raw_data\\{opc_id}',
                                           f'raw_data\\{opc_id}\\OP {op_id}')
                    df = read_welas_spd(f, calibration_file, tz)
                spd_df.append(df)
            spd_df_all = pd.concat(spd_df)

            df_hk_instrument_period = df_hk_instrument.loc[start_m2:stop_p1, :]

            fig, (ax, ax2) = plt.subplots(2, 1, figsize=(16, 9),
                                          constrained_layout=True,
                                          sharex=True)
            for sensor in ['Ti1', 'Ti2', 'Ti3', 'Ti4', 'Ti5']:
                ax.plot(df_hk_instrument_period.index,
                        df_hk_instrument_period[sensor],
                        label=sensor, linestyle='-',
                        linewidth=2)
            for senosr in ['Tw1', 'Tw2', 'Tw3']:
                ax.plot(df_hk_instrument_period.index,
                        df_hk_instrument_period[sensor],
                        label=sensor, linestyle='-.',
                        linewidth=2)
            ax.plot(df_hk_instrument_period.index,
                    df_hk_instrument_period['DP'],
                    label=r'$T_\mathrm{dp}$', linestyle='--',
                    linewidth=2)
            ax.axvspan(start_m1, start_0, alpha=0.5)
            ax.set_ylabel('$T$ / °C')
            secax = ax.secondary_yaxis('right', functions=(lambda x: x+273.15,
                                                           lambda x: x-273.15))
            secax.set_ylabel('$T$ / K')
            ax.grid()
            ax.set_title(f'{pine_id} ({campaign}): operation {op_id}, run_id {run_id}',
                         pad=10)
            ax.legend(ncol=2)

            ax2.plot(df_hk_instrument_period.index,
                     df_hk_instrument_period['Pch'],
                     label='$p$', color='C0')
            ax2.set_ylabel('$p$ / hPa')
            ax2_twin = ax2.twinx()
            ax2_twin.scatter(spd_df_all.index, spd_df_all['dp / um'],
                             c=spd_df_all['Ti / us'], s=0.1)
            try:
                ax2_twin.plot((start_m2,
                               start_m1),
                              (ice_threshold.loc[ice_threshold['run id'] == run_id-1,
                                                 'd_min'].to_numpy()[0],
                               ice_threshold.loc[ice_threshold['run id'] == run_id-1,
                                                 'd_min'].to_numpy()[0]),
                              color='C1')
            except IndexError as e:
                print(f'IndexError: {e}; First run of the current operation is selected.')
            ax2_twin.plot((start_m1,
                           start_0),
                          (ice_threshold.loc[ice_threshold['run id'] == run_id,
                                             'd_min'].to_numpy()[0],
                           ice_threshold.loc[ice_threshold['run id'] == run_id,
                                             'd_min'].to_numpy()[0]),
                          color='C2')
            try:
                ax2_twin.plot((start_0,
                               stop_p1),
                              (ice_threshold.loc[ice_threshold['run id'] == run_id+1,
                                                 'd_min'].to_numpy()[0],
                               ice_threshold.loc[ice_threshold['run id'] == run_id+1,
                                                 'd_min'].to_numpy()[0]),
                              color='C3')
            except IndexError as e:
                print(f'IndexError: {e}; Last run of the current operation is selected.')

            ax2_twin.set_yscale('log')
            ax2_twin.set_ylabel(r'$d_\mathrm{p}$ / um')
            locator = mdates.AutoDateLocator()
            formatter = mdates.ConciseDateFormatter(locator)
            ax2_twin.xaxis.set_major_locator(locator)
            ax2_twin.xaxis.set_major_formatter(formatter)
            ax2_twin.grid(True, which='both', axis='both')

        draw_diagnostic(idx)
    fig.canvas.mpl_connect('pick_event', onpick)
    fig.canvas.mpl_connect('key_press_event', on_key)

    plt.show()


def manual_flagging(pine_data: pd.DataFrame,
                    run_issues_file_name: typing.TextIO) -> list:
    """
    Function that creates a list of index to be flagged according to a file.

    Parameters
    ----------
    pine_data : pd.DataFrame
        PINE data as returned by the function read_pine_data().
    run_issues_file_name : typing.TextIO
        Name of the file which lists manual flaggings.

    Returns
    -------
    list
        List that contains all manually flagged index.

    """
    import numpy as np

    def parse_tuple(string):
        import ast
        try:
            s = ast.literal_eval(str(string))
            if isinstance(s, tuple):
                return s
            else:
                return int(string)
        except Exception:
            return

    run_issues = pd.read_csv(run_issues_file_name,
                             sep='\t')
    run_issues.loc[:, 'run_id'] = run_issues.loc[:, 'run_id'].map(parse_tuple)

    idx_removal = []
    for op_id in run_issues['op_id'].unique():
        run_issues_per_op_id = run_issues.loc[run_issues['op_id'] == op_id, :].copy(deep=True)
        # check if new run issues has length 1 or more
        if len(run_issues_per_op_id.index) == 1:
            # check if run_id is tuple or int
            run_id = run_issues_per_op_id['run_id'].to_numpy()[0]
            if isinstance(run_id, (int, np.integer)):
                mask = pine_data.loc[(pine_data['op_id'] == op_id)
                                     & (pine_data['run_id'] == run_id), :]
            elif isinstance(run_id, tuple):
                # if tuple is length 1 remove everything higher and equal than run number given
                if len(run_id) == 1:
                    mask = (pine_data.loc[pine_data['op_id'] == op_id, :]
                            .loc[pine_data['run_id'] >= run_id[0]])
                # if tuple length 2, remove everything between the range as well as the end points
                elif len(run_id) == 2:
                    mask = (pine_data.loc[(pine_data['op_id'] == op_id)
                                          & (pine_data['run_id'] >= run_id[0])
                                          & (pine_data['run_id'] <= run_id[1]), :])
            mask = mask.index.to_list()
        # if there are more than one line, we need to iterate through the lines
        elif len(run_issues_per_op_id.index) > 1:
            mask_lst = []
            for idx, row in run_issues_per_op_id.iterrows():
                # check if run_id is tuple or int
                run_id = row['run_id']
                if isinstance(run_id, int):
                    mask = pine_data.loc[(pine_data['op_id'] == op_id)
                                         & (pine_data['run_id'] == run_id), :]
                elif isinstance(run_id, tuple):
                    # if tuple is length 1 remove everything higher and equal than run number given
                    if len(run_id) == 1:
                        mask = (pine_data.loc[pine_data['op_id'] == op_id, :]
                                .loc[pine_data['run_id'] >= run_id[0]])
                    # if tuple length 2, remove everything between the range as well as
                    # the end points
                    elif len(run_id) == 2:
                        mask = (pine_data.loc[(pine_data['op_id'] == op_id)
                                              & (pine_data['run_id'] >= run_id[0])
                                              & (pine_data['run_id'] <= run_id[1]), :])
                mask_lst.append(mask.index.to_list())
            mask = [item for sublist in mask_lst for item in sublist]
        idx_removal.append(mask)

    idx_removal = [item for sublist in idx_removal for item in sublist]
    return idx_removal
